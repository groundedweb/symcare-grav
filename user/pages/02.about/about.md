---
title: About
heroImage: hands-1150073_1920.jpg
Header1: '"Love begins at home, it doesn''t mater how much we do... but how much love we put into the action." - Mother Theresa'
MissionTitle: 'Our Misson'
MissionList:
    -
        text: 'Our goal is to provide outstanding and customized care by working together with you to guarantee your well being and safety, support you independence and enhance your quality of life.'
    -
        text: 'Whether you need companionship, light housekeeping or need more focused care due to Alzheimer''s or dementia we are here to help. Let us help you live every day to the fullest'
Team:
    -
        name: 'Gedewon Kassa'
        photo: team-placeholder.jpg
        title: Director
    -
        name: 'Shavon Land'
        photo: team-placeholder.jpg
        title: Administrator
    -
        name: 'Demetriana Mclendon'
        photo: team-placeholder.jpg
        title: 'Client Care Coordinator'
    -
        name: 'Deloria Sander'
        photo: team-placeholder.jpg
        title: 'Client Care Coordinator'
    -
        name: 'Kirk Codrington'
        photo: team-placeholder.jpg
        title: 'Client Care Coordinator'
    -
        name: 'Jayda Maxwell'
        photo: team-placeholder.jpg
        title: 'Intern Office Assistant'
Client:
    -
        title: 'Client 1'
        img: team-placeholder.jpg
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce lobortis volutpat ex eget scelerisque. Donec.'
    -
        title: 'Client 2'
        img: team-placeholder.jpg
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce lobortis volutpat ex eget scelerisque. Donec.'
    -
        title: 'Client 3'
        img: team-placeholder.jpg
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce lobortis volutpat ex eget scelerisque. Donec.'
MissonTitle: 'Our Misson'
About:
    -
        title: 'Title 1'
        img: medic-hero.jpg
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce lobortis volutpat ex eget scelerisque. Donec.'
    -
        title: 'Title 2'
        img: medic-hero.jpg
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce lobortis volutpat ex eget scelerisque. Donec.'
    -
        title: 'Title 3'
        img: medic-hero.jpg
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce lobortis volutpat ex eget scelerisque. Donec.'
HeroText1: 'Our goal is to provide outstanding and customized care by working together with you to guarantee your well being and safety, support your independence and enhance your quality of life'
---

